import matplotlib.pyplot as plt;
import pandas as pd;

data = pd.read_csv("iris.csv")

#print(data.describe())

is_setosa = data['class'] == 'Iris-setosa'
is_versicolor = data['class'] == 'Iris-versicolor'
is_virginica = data['class'] == 'Iris-virginica'

iris_setosas = data[is_setosa]
iris_versicolor = data[is_versicolor]
iris_virginica = data[is_virginica]


fig = plt.figure()
ax1 = fig.add_subplot(111)

#ax1.scatter(x=midsize_cars['EngineSize'], y=midsize_cars['MPG.highway'])
ax1.scatter(x=iris_setosas['sepal_length'], y=iris_setosas['sepal_width'] , color=['red'])
ax1.scatter(x=iris_versicolor['sepal_length'], y=iris_versicolor['sepal_width'] , color=['purple'])
ax1.scatter(x=iris_virginica['sepal_length'], y=iris_virginica['sepal_width'] , color=['green'])
plt.show()


#print(iris_setosas)


#data.plot(x='class', y='is_setosa', kind = 'scatter').plt.show()
#data.plot(x='sepa_length', y='sepal_width', kind = 'scatter').plt.show()